<?php

Route::get('/', function () {
    return view('index');
});
Route::get('/LogIn', function () {
    return view('sign');
});
Route::get('/Gallery', function () {
    return view('gallery');
});
Route::get('/Contacts', function () {
    return view('contact');
});
Route::get('/info', function () {
    return view('detail');
});
Route::get('/register', function(){
	return view('auth.register');
});
//Route::get('/register', function(){
//	return view('auth.login');
//});
/*-------*/

Route::match(['get', 'post'], '/admin', 'AdminController@login');



Route::get('/logout', 'AdminController@logout');

Route::group(['middleware' => ['auth']], function(){
	Route::get('/admin/dashboard', 'AdminController@dashboard');
	Route::get('/admin/settings', 'AdminController@settings');
	Route::get('/admin/check-pwd', 'AdminController@chkPassword');
	Route::match(['get', 'post'], '/admin/update-pwd', 'AdminController@updatePassword');

	Route::match(['get', 'post'], '/admin/add-category', 'CategoryController@addCategory');
	Route::match(['get', 'post'], '/admin/edit-category/{id}', 'CategoryController@editCategory');
	Route::match(['get', 'post'], '/admin/delete-category/{id}', 'CategoryController@deleteCategory');
	Route::get('/admin/view-category', 'CategoryController@viewCategory');

	/*Product Routes*/
	Route::match(['get', 'post'], '/admin/add-product', 'ProductController@addProduct');
	Route::match(['get', 'post'], '/admin/view-product', 'ProductController@viewProduct');
	Route::match(['get', 'post'], '/admin/edit-product/{id}', 'ProductController@editProduct');
	Route::get('/admin/delete-product/{id}','ProductController@deleteProduct');

});

/*---------*/
Route::get('/Gallery','UserController@show');

Route::get('/home/{id}','UserController@showItem');
/*---------*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
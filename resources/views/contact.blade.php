<!DOCTYPE html> 
<html lang="en">
<head>
	<title>Contacts</title>	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/w.css">
</head>
<body>
	<section class="intro">
		<nav>
			<a href="#" id="menu-icon"></a>
				<ul>
                    <li><a href='/'>Home</a></li>
					<li><a href='/admin'>Sign In</a></li>
				    <li><a href='/Gallery'>Gallery</a></li>
				    <li><a href='/Contacts'>Contact Me</a></li>
				</ul>
		</nav>
		<div class="inner">
		    <div class="content">
		      <h1>Contacts</h1>
		    </div>
	  	</div>
	</section>

  <section class="section section-light">
  	<p>Moldagaliyeva Yenlik  <a href="mailto:yenlikmol@gmail.com">yenlikmol@gmail.com</a>  8 (708) 847-86-33</p>
	<p>1000 Street Road Almaty My State 19000</p>


	<ul class="social">
		<li><a href="https://plus.google.com/u/0/102615018059862711806" target="_blank"><i class="fa fa-google-plus"></i></a></li>
		<li><a href="https://twitter.com/Yena68977285" target="_blank"><i class="fa fa-twitter"></i></a></li>
		<li><a href="https://www.instagram.com/v_victory_t/" target="_blank"><i class="fa fa-instagram"></i></a></li>
	</ul>
  </section>
  <footer class="second">
  	<p>&copy; Copyright.</p>
  </footer>

	
</body>
</html>

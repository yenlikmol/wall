<!DOCTYPE html>
<html lang="en">
<head>
<title>Matrix Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="/css/backend_css/bootstrap.min.css" />
<link rel="stylesheet" href="/css/backend_css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="/css/backend_css/fullcalendar.css" />
<link rel="stylesheet" href="/css/backend_css/matrix-style.css" />
<link rel="stylesheet" href="/css/backend_css/matrix-media.css" />
<link rel="stylesheet" href="/css/backend_css/select2.css" />
<link rel="stylesheet" href="/css/backend_css/uniform.css" />
<link rel="stylesheet" href="/css/backend_css/colorpicker.css" />
<link rel="stylesheet" href="/css/backend_css/datepicker.css" />
<link rel="stylesheet" href="/css/backend_css/bootstrap-wysihtml5.css" />
<link href="/fonts/backend_font/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/backend_css/jquery.gritter.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>


@include('layouts.adminLayout.admin_header')

@include('layouts.adminLayout.admin_sidebar')

@yield('content')


@include('layouts.adminLayout.admin_footer')

<script src="/js/backend_js/jquery.min.js"></script> 
<script src="/js/backend_js/jquery.ui.custom.js"></script> 
<script src="/js/backend_js/bootstrap.min.js"></script> 
<script src="/js/backend_js/jquery.uniform.js"></script> 
<script src="/js/backend_js/select2.min.js"></script> 
<script src="/js/backend_js/jquery.dataTables.min.js"></script> 
<script src="/js/backend_js/jquery.validate.js"></script> 
<script src="/js/backend_js/matrix.js"></script> 
<script src="/js/backend_js/matrix.form_validation.js"></script>
<script src="/js/backend-js/matrix.tables.js"></script>
<script src="/js/backend_js/matrix.interface.js"></script> 
<script src="/js/backend_js/matrix.popover.js"></script>
<script src="/js/backend_js/jquery.gritter.min.js"></script> 
</body>
</html>

<!DOCTYPE html> 
<html lang="en">
<head>
	<title>Gallery</title>	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/w.css">

  	<link rel="stylesheet" href="/css/font-awesome.css">
	<link rel="stylesheet" href="/home/yenlik/store/public/css/animate.css">
 	<link rel="stylesheet" href="/home/yenlik/store/public/css/templatemo_misc.css">
	<link rel="stylesheet" href="/home/yenlik/store/public/css/templatemo_style.css">
  
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/sort.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    
</head>
<body>
  <section class="intro">
      <nav>
			<a href="#" id="menu-icon"></a>
			<ul>
                <li><a href='/'>Home</a></li>
				<li><a href='/admin'>Sign In</a></li>
				<li><a href='/Gallery'>Gallery</a></li>
				<li><a href='/Contacts'>Contact Me</a></li>
			</ul>
      </nav>
  	<div class="inner">
      <div class="content">
            <h1>Gallery</h1> 
      </div>
    </div>
  </section>
<div class="container">
    @foreach($item->chunk(3) as $var)
    <div class="row">
     @foreach($var as $var1)
      <div class="col-md-4">
        <figure class="snip1246">
          <img src="/img/{{$var1->image}}" alt="sample88"/>
          <figcaption>
            <h2>{{$var1->product_name}}</h2>
            <p>{{$var1->desctiption}}</p>
            <div class="price">${{$var1->price}}</div>
            <a href='home/{{$var1->id}}' class="add-to-cart">View more</a>
          </figcaption>
        </figure>
      </div>
      @endforeach
    </div>
    @endforeach
  {{$item->links()}}

  <footer class="second">
    <p>&copy; Copyright.</p>
  </footer>

	
</body>
</html>

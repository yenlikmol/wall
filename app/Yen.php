<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Yen extends Model
{
    //
    protected $table = 'pics';
    public $timestamps = false;
    protected $dateFormat = 'U';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';
}

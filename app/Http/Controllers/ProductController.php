<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input; 
use App\Category;
use Auth;
use Image;
use Session;
use App\Product;

class ProductController extends Controller
{
    public function addProduct(Request $request){

    	if($request->isMethod('post')){
    		$data = $request->all();
    		$product = new Product;
    		$product->category_id = $data['category_id'];
    		$product->product_name = $data['prod_name'];
    		$product->product_code = $data['prod_code'];
    		if(!empty($data['description'])){
    			$product->description = $data['description'];
    		}else{
    			$product->description = '';
    		}
    		$product->price = $data['price'];
    		//Upload Image
    		$product->image = $data['prod_img'];
    		$product->save();
    		return redirect('/admin/view-product')->with('flash_message_success', 'Product was added Successfully!');
    	}


    	$categories = Category::where(['parent_id'=>0])->get();
    	$categories_dropdown = "<option value='' selected disabled>Select</option>";
    	foreach($categories as $cat){
    		$categories_dropdown .= "<option value='".$cat->id."'>".$cat->name."</option>";
    		$sub_categories = Category::where(['parent_id'=>$cat->id])->get();
    		foreach ($sub_categories as $sub_cat) {
    			$categories_dropdown .= "<option value = '".$sub_cat->id."'>&nbsp;--&nbsp;".$sub_cat->name."</option>";
    		}
    	}


    	return view('admin.products.add_product')->with(compact('categories_dropdown'));

    }

    public function viewProduct(Request $request){
    	$products = Product::get();
    	$products = json_decode(json_encode($products));
    	foreach ($products as $key => $val) {
    		$category_name = Category::where(['id'=>$val->category_id])->first();
    		$products[$key]->category_name = $category_name->name;
    	}
    	//echo "<pre>"; print_r($products);die;
    	return view('admin.products.view_product')->with(compact('products'));
    }

    public function editProduct(Request $request, $id = null){

    	if($request->isMethod('post')){
    		$data = $request->all();
    		//echo "<pre>"; print_r($data); die;
    		Product::where(['id'=>$id])->update(['category_id' => $data['category_id'],'product_name'=>$data['prod_name'], 'product_code'=>$data['prod_code'],'description'=>$data['description'],'price'=> $data['price']]);
    		return redirect()->back()->with('flash_message_success', 'Product was updated Successfully!');
    	}

    	$productDetails = Product::where(['id' => $id])->first();

    	$categories = Category::where(['parent_id'=>0])->get();
    	$categories_dropdown = "<option value='' selected disabled>Select</option>";
    	foreach($categories as $cat){
    		if($cat->id==$productDetails->category_id){
    			$selected = "selected";
    		}else{
    			$selected = "";
    		}
    		$categories_dropdown .= "<option value='".$cat->id."' ".$selected.">".$cat->name."</option>";
    		$sub_categories = Category::where(['parent_id'=>$cat->id])->get();
    		foreach ($sub_categories as $sub_cat) {
    				if($sub_cat->id==$productDetails->category_id){
    			$selected = "selected";
    		}else{
    			$selected = "";
    		}
    			$categories_dropdown .= "<option value = '".$sub_cat->id."' ".$selected.">&nbsp;--&nbsp;".$sub_cat->name."</option>";
    		}
    	}
    	return view('admin.products.edit_product')->with(compact('productDetails', 'categories_dropdown'));
    }

    public function deleteProduct($id = null){
    	Product::where(['id' => $id])->delete();
    	return redirect()->back()->with('flash_message_success', 'Product deleted Successfully!');

    }
}

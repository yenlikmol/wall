<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
	public function login(Request $request){
		if($request->isMethod('post')){
			$data = $request->input();
			if(Auth::attempt(['email'=>$data['e-mail'],'password'=>$data['password'],'admin'=>'1'])){ 
				
				return redirect('/admin/dashboard');
			}else{
				return redirect('/admin')->with('flash_message_error', 'Invalid Username or Password');
			}
		}
		return view('admin.admin_login');
	}

	public function dashboard(){
	
		return view('admin.dashboard');
	}
	public function settings(){
		return view('admin.settings');
	}

	public function logout(){
		Session::flush();
		return redirect('/admin')->with('flash_message_success', 'Logged out Successfully');
	}

	public function chkPassword(Requeest $request){
		$data = $request->all();
		$current_password = $data['current_pwd'];
		$check_password = User::where(['admin' => '1'])->first();
		if(Hash::check($current_password, $check_password->password)){
			echo "true"; die;
		}else{
			echo "false"; die;
		}
	}

	public function updatePassword(Request $request){
		if($request->isMethod('post')){
			$data = $request->all();
			//echo "pre"; print_r($data); die;
			$check_password = User::where(['email' => Auth::user()->email])->first();
			$current_password = $data['current_pwd'];
			if(Hash::check($current_password, $check_password->password)){
				$password = bcrypt($data['new_pwd']);
				User::where('id', '1')->update(['password'=>$password]);

			}else{
				return redirect('/admin/settings')->with('flash_message_error', 'Incorrect Current Password');
			}
		}

	}

	/*public function create(){
	    $categories=category::all(['category_id','cat_name']);   
	     	return view('admin.create', compact('category',$categories));
	    $product_items=products::all(['prodId','prodName']);   
	     	return view('admin.store', compact('products',$product_items));	   
   	}
    public function store(){
     //$store_cat=new categories();
     $store_prod=new products();
     $store_prod-> id_prod = Input::get("prodId"); 
     //$store_prod  -> co_cat_id = Input::get("productSelect");
     $store_prod -> name_product = Input::get("prodName");
     $store_prod -> cost_product = Input::get("prodCost"); 
     //$store_prod -> characteristic = Input::get("comment"); 
     $store_prod ->save();  
      return redirect('/admin/store');
     	   
   }
       public function storePicture(){
       	$product_items=products::all(['prodId','prodName']);   
        return view('admin.store', compact('products',$product_items));	   
   } 
    public function addPicture(Request $request){
    	if ($request->hasFile('pic')) {
    		$filename=$request->file('pic')->getClientOriginalName();
    		 $request->file('picture')->storeAs('./public/img',$filename);

    		 $picture=new picture;
    		 $picture->path_pic=$filename;
    		 $picture->name_pic=$filename;
    		 $picture->id_prod=Input::get("productSelect");
    		 $picture->save();
    		 return redirect('adminpanel/store');
    		
    	}
       	 
   }*/
}

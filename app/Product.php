<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  public $table ='product';
    public $primaryKey='id';
    public $timestamps=false;
    public function products(){
   	 	return $this->belongsTo('App\product','id');
    }

}
